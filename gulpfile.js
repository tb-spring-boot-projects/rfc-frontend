const gulp = require('gulp');
const sass = require('gulp-sass');
const clean = require('gulp-clean');
const rename = require('gulp-rename');
const cleanCSS = require('gulp-clean-css');
const zip = require('gulp-zip');
const {version} = require('./package.json');
const browserSync = require('browser-sync');
const nodemon = require('nodemon');
const watch = require('gulp-watch');


/* Remove all compiled files */
function cleanDist() {
    return gulp.src('dist', {allowEmpty: true})
        .pipe(clean());
}

/**
 * CSS tasks
 */

sass.compiler = require('sass');

/* Build the CSS from source */
function compileCSS() {
    return gulp.src(['packages/ravens.scss'])
        .pipe(sass())
        .pipe(gulp.dest('dist/css/'))
        .on('error', (err) => {
            console.log(err);
            process.exit(1);
        });
}

/* Minify CSS and add a min.css suffix */
function minifyCSS() {
    return gulp.src([
        'dist/css/*.css',
        '!dist/css/*.min.css', // don't re-minify minified css
    ])
        .pipe(cleanCSS())
        .pipe(rename({
            suffix: `-${version}.min`,
        }))
        .pipe(gulp.dest('dist/css/'));
}


/**
 * Assets tasks
 */

/**
 * Copy assets such as icons and images into the distribution
 */
function assets() {
    return gulp.src('packages/assets/**')
        .pipe(gulp.dest('dist/assets/'));
}

/**
 * Release tasks
 */


/* Copy CSS files into their relevant folders */

function cssFolder() {
    return gulp.src('dist/css/*.min.css')
        .pipe(clean())
        .pipe(gulp.dest('dist/css/'));
}

function createZip() {
    return gulp.src(['dist/css/*.min.css', 'dist/assets/**'], {base: 'dist'})
        .pipe(zip(`rfc-frontend-${version}.zip`))
        .pipe(gulp.dest('dist'));
}

function reload(done) {
    compileCSS();
    assets();
    browserSync.reload();
    done();
}

/**
 * Development tasks
 */
gulp.task('clean', cleanDist);

gulp.task('style', compileCSS);

gulp.task('build', gulp.series([
    compileCSS, assets,
]));

gulp.task('bundle', gulp.series([
    cleanDist,
    'build',
    minifyCSS,
]));

gulp.task('zip', gulp.series([
    'bundle',
    assets,
    cssFolder,
    createZip,
]));


// Nodemon task:
// Start nodemon once and execute callback (browser-sync)
gulp.task('nodemon', cb => {
    let started = false;
    return nodemon({
        script: 'index.js'
    }).on('start', () => {
        if (!started) {
            cb();
            started = true;
        }
    });
});
gulp.task('reload', reload)

// BrowserSync task:
// calls nodemon tasks and pass itself as callback
gulp.task('browser-sync', () => {
    watch(['./templates/**/*'], (done) => {gulp.series(['reload'])(done)});
    watch(['./packages/**/*'], (done) => {gulp.series(['reload'])(done)});
    browserSync.init({proxy: 'localhost:8000'})
});

/**
 * The default task is to build everything and watch for changes
 */
gulp.task('default', gulp.parallel(
    'nodemon',
    'browser-sync'
));
